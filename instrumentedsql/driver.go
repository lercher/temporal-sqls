package instrumentedsql

// adapted from https://github.com/ExpansiveWorlds/instrumentedsql
// removed Logger and Tracer

import (
	"context"
	"database/sql/driver"
	"fmt"
	"log"
	"strings"

	"github.com/pkg/errors"
)

type wrappedDriver struct {
	parent driver.Driver
}

type wrappedConn struct {
	parent driver.Conn
}

type wrappedTx struct {
	ctx    context.Context
	parent driver.Tx
}

type wrappedStmt struct {
	ctx    context.Context
	query  string
	parent driver.Stmt
}

type wrappedResult struct {
	ctx    context.Context
	parent driver.Result
}

type wrappedRows struct {
	ctx    context.Context
	parent driver.Rows
}

// WrapDriver wraps the passed SQL driver and returns a new sql driver that uses it.
// Note that driver.Driver is an interface.
//
// Standard use case for wrapping e.g. mssql:
//
// once.Do(func() {
//   driver := instrumentedsql.WrapDriver(&mssql.Driver{})
//   sql.Register("sqlserver-i", driver)
// })
// ...
// db, err := sql.Open("sqlserver-i", dsn)
func WrapDriver(driver driver.Driver) driver.Driver {
	return wrappedDriver{parent: driver}
}

func (d wrappedDriver) Open(name string) (driver.Conn, error) {
	conn, err := d.parent.Open(name)
	lerr(err)
	return wrappedConn{parent: conn}, err
}

func (c wrappedConn) Close() error {
	return c.parent.Close()
}

func (c wrappedConn) Begin() (driver.Tx, error) {
	log.Println("BEGIN")
	tx, err := c.parent.Begin()
	lerr(err)
	return wrappedTx{parent: tx}, err
}

func (c wrappedConn) BeginTx(ctx context.Context, opts driver.TxOptions) (tx driver.Tx, err error) {
	if connBeginTx, ok := c.parent.(driver.ConnBeginTx); ok {
		log.Println("BEGIN-TX", ctxDeadline(ctx))
		tx, err = connBeginTx.BeginTx(ctx, opts)
		lerr(err)
		return wrappedTx{ctx: ctx, parent: tx}, err
	}

	tx, err = c.parent.Begin()
	return wrappedTx{ctx: ctx, parent: tx}, err
}

func (c wrappedConn) Prepare(query string) (driver.Stmt, error) {
	parent, err := c.parent.Prepare(query)
	lerr(err)
	return wrappedStmt{query: query, parent: parent}, err
}

func (c wrappedConn) PrepareContext(ctx context.Context, query string) (stmt driver.Stmt, err error) {
	if connPrepareCtx, ok := c.parent.(driver.ConnPrepareContext); ok {
		stmt, err := connPrepareCtx.PrepareContext(ctx, query)
		lerr(err)
		return wrappedStmt{ctx: ctx, query: query, parent: stmt}, err
	}

	return c.Prepare(query)
}

func (c wrappedConn) Exec(query string, args []driver.Value) (driver.Result, error) {
	if execer, ok := c.parent.(driver.Execer); ok {
		log.Println("EXEC", query)
		res, err := execer.Exec(query, args)
		lerr(err)
		return wrappedResult{parent: res}, err
	}

	return nil, driver.ErrSkip
}

func (c wrappedConn) ExecContext(ctx context.Context, query string, args []driver.NamedValue) (r driver.Result, err error) {
	if execContext, ok := c.parent.(driver.ExecerContext); ok {
		log.Println("EXEC-CTX", ctxDeadline(ctx), query)
		res, err := execContext.ExecContext(ctx, query, args)
		lerr(err)
		return wrappedResult{ctx: ctx, parent: res}, err
	}

	// Fallback implementation
	dargs, err := namedValueToValue(args)
	if err != nil {
		return nil, err
	}

	select {
	default:
	case <-ctx.Done():
		return nil, ctx.Err()
	}

	return c.Exec(query, dargs)
}

func (c wrappedConn) Ping(ctx context.Context) (err error) {
	if pinger, ok := c.parent.(driver.Pinger); ok {
		return pinger.Ping(ctx)
	}
	return nil
}

func (c wrappedConn) Query(query string, args []driver.Value) (driver.Rows, error) {
	if queryer, ok := c.parent.(driver.Queryer); ok {
		log.Println("QUERY", query)
		rows, err := queryer.Query(query, args)
		lerr(err)
		return wrappedRows{parent: rows}, err
	}

	return nil, driver.ErrSkip
}

func (c wrappedConn) QueryContext(ctx context.Context, query string, args []driver.NamedValue) (rows driver.Rows, err error) {
	if queryerContext, ok := c.parent.(driver.QueryerContext); ok {
		log.Println("QUERY-CTX", ctxDeadline(ctx), query)
		rows, err := queryerContext.QueryContext(ctx, query, args)
		lerr(err)
		return wrappedRows{ctx: ctx, parent: rows}, err
	}

	dargs, err := namedValueToValue(args)
	if err != nil {
		return nil, err
	}

	select {
	default:
	case <-ctx.Done():
		return nil, ctx.Err()
	}

	return c.Query(query, dargs)
}

func (t wrappedTx) Commit() (err error) {
	log.Println("COMMIT")
	return t.parent.Commit()
}

func (t wrappedTx) Rollback() (err error) {
	log.Println("ROLLBACK")
	return t.parent.Rollback()
}

func (s wrappedStmt) Close() (err error) {
	return s.parent.Close()
}

func (s wrappedStmt) NumInput() int {
	return s.parent.NumInput()
}

func (s wrappedStmt) Exec(args []driver.Value) (res driver.Result, err error) {
	log.Println("S-EXEC", s.query, args)
	res, err = s.parent.Exec(args)
	lerr(err)
	return wrappedResult{ctx: s.ctx, parent: res}, err
}

func (s wrappedStmt) ExecContext(ctx context.Context, args []driver.NamedValue) (res driver.Result, err error) {
	if stmtExecContext, ok := s.parent.(driver.StmtExecContext); ok {
		log.Println("S-EXEC-CTX", ctxDeadline(ctx), s.query, args)
		res, err := stmtExecContext.ExecContext(ctx, args)
		lerr(err)
		return wrappedResult{ctx: ctx, parent: res}, err
	}

	// Fallback implementation
	dargs, err := namedValueToValue(args)
	if err != nil {
		return nil, err
	}

	select {
	default:
	case <-ctx.Done():
		return nil, ctx.Err()
	}

	return s.Exec(dargs)
}

func (s wrappedStmt) Query(args []driver.Value) (rows driver.Rows, err error) {
	log.Println("S-QUERY", s.query, args)
	lerr(err)
	rows, err = s.parent.Query(args)
	if err != nil {
		return nil, err
	}

	return wrappedRows{ctx: s.ctx, parent: rows}, nil
}

func (s wrappedStmt) QueryContext(ctx context.Context, args []driver.NamedValue) (rows driver.Rows, err error) {
	if stmtQueryContext, ok := s.parent.(driver.StmtQueryContext); ok {
		log.Println("S-QUERY-CTX", ctxDeadline(ctx), s.query, args)
		rows, err := stmtQueryContext.QueryContext(ctx, args)
		lerr(err)
		return wrappedRows{ctx: ctx, parent: rows}, err
	}

	dargs, err := namedValueToValue(args)
	if err != nil {
		return nil, err
	}

	select {
	default:
	case <-ctx.Done():
		return nil, ctx.Err()
	}

	return s.Query(dargs)
}

func (r wrappedResult) LastInsertId() (id int64, err error) {
	return r.parent.LastInsertId()
}

func (r wrappedResult) RowsAffected() (num int64, err error) {
	return r.parent.RowsAffected()
}

func (r wrappedRows) Columns() []string {
	return r.parent.Columns()
}

func (r wrappedRows) Close() error {
	return r.parent.Close()
}

func (r wrappedRows) Next(dest []driver.Value) (err error) {
	return r.parent.Next(dest)
}

// namedValueToValue is a helper function copied from the database/sql package
func namedValueToValue(named []driver.NamedValue) ([]driver.Value, error) {
	dargs := make([]driver.Value, len(named))
	for n, param := range named {
		if len(param.Name) > 0 {
			return nil, errors.New("sql: driver does not support the use of Named Parameters")
		}
		dargs[n] = param.Value
	}
	return dargs, nil
}

// ctxDeadline extracts a log-able description of the ctx's deadline
func ctxDeadline(ctx context.Context) string {
	if dl, ok := ctx.Deadline(); ok {
		return fmt.Sprintf("[deadline %v]", dl.Format("15:04:05"))
	}
	return "(no deadline)"
}

func lerr(err error) {
	if err != nil {
		txt := err.Error()
		log.Println("ßßßßß", txt) // ßßßßß as a marker to filter for these log lines in VS Code's DEBUG CONSOLE window
		switch {
		default:
			// serves as good breakpoint: this unknown error text can be a problem and probably needs to be investigated
			_ = 0

		// ------------------------------------------------------------------- implementation bugs ---------------------------------
		case strings.HasPrefix(txt, "mssql: Incorrect syntax near"):
			panic(txt) // implementation bug
		case strings.HasPrefix(txt, "mssql: Must declare the scalar variable"):
			panic(txt) // implementation bug
		case strings.HasPrefix(txt, "mssql: String or binary data would be truncated"):
			panic(txt) // implementation bug

		// ------------------------------------------------------------------- expectable errors ---------------------------------
		case strings.HasPrefix(txt, "context deadline exceeded"):
			// can happen, needs no breakpoint
		case strings.HasPrefix(txt, "mssql: Violation of PRIMARY KEY constraint"):
			// can happen, esp. in test suite, needs no breakpoint
		case strings.HasPrefix(txt, "mssql: Violation of UNIQUE KEY constraint"):
			// can happen, esp. in test suite, needs no breakpoint
		}
	}
}
