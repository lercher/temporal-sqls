-- mockup schema\temporal\schema.sql for test suite to delete all data except delete from temporal.schema_version

delete from temporal.schema_update_history;
delete from temporal.namespaces;
delete from temporal.shards;
delete from temporal.executions;
delete from temporal.current_executions;
delete from temporal.buffered_events;
delete from temporal.tasks;
delete from temporal.task_queues;
delete from temporal.transfer_tasks;
delete from temporal.timer_tasks;
delete from temporal.replication_tasks;
delete from temporal.replication_tasks_dlq;
delete from temporal.visibility_tasks;
delete from temporal.activity_info_maps;
delete from temporal.timer_info_maps;
delete from temporal.child_execution_info_maps;
delete from temporal.request_cancel_info_maps;
delete from temporal.signal_info_maps;
delete from temporal.signals_requested_sets;
delete from temporal.history_node;
delete from temporal.history_tree;
delete from temporal.queue;
delete from temporal.queue_metadata;
delete from temporal.cluster_metadata_info;
delete from temporal.cluster_membership;

delete from temporal.namespace_metadata;
insert into temporal.namespace_metadata (partition_id, notification_version) VALUES (54321, 1);
