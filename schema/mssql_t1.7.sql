-- CREATE DATABASE temporal
-- use temporal
create SCHEMA temporal
go
CREATE USER temporal WITHOUT LOGIN WITH DEFAULT_SCHEMA = temporal;
go
sp_addrolemember 'db_owner', 'temporal'
GO

execute as user='temporal';
---------------------------------------------------- this code converted online by http://www.sqlines.com/online
---------------------------------------------------- see also http://www.sqlines.com/download

CREATE TABLE schema_version(
	version_partition INT not null,
	db_name VARCHAR(255) not null,
	creation_time DATETIME2(6),
	curr_version VARCHAR(64),
	min_compatible_version VARCHAR(64),
	PRIMARY KEY (version_partition, db_name)
);

INSERT into schema_version
  (version_partition, db_name, creation_time, curr_version, min_compatible_version) 
	VALUES 
  (0,                 DB_NAME(), GETDATE(),  '1.4',        '1.4') -- for main 1.4 and visibility 1.1
;

CREATE TABLE schema_update_history(
	version_partition INT not null, 
	year int not null, 
	month int not null, 
	update_time DATETIME2(6) not null, 
	description VARCHAR(255), 
	manifest_md5 VARCHAR(64), 
	new_version VARCHAR(64), 
	old_version VARCHAR(64), 
	PRIMARY KEY (version_partition, year, month, update_time)
);

---------------------------------------------------- from https://github.com/temporalio/temporal/tree/master/schema/postgresql v96

CREATE TABLE namespaces(
  partition_id INTEGER NOT NULL,
  id BINARY(16) NOT NULL,
  name VARCHAR(255) UNIQUE NOT NULL,
  notification_version BIGINT NOT NULL,
  --
  data VARBINARY(max) NOT NULL,
  data_encoding VARCHAR(16) NOT NULL,
  is_global BIT NOT NULL,
  PRIMARY KEY(partition_id, id)
);

CREATE TABLE namespace_metadata (
  partition_id INTEGER NOT NULL,
  notification_version BIGINT NOT NULL,
  PRIMARY KEY(partition_id)
);

INSERT INTO namespace_metadata (partition_id, notification_version) VALUES (54321, 1);

CREATE TABLE shards (
  shard_id INTEGER NOT NULL,
  --
  range_id BIGINT NOT NULL,
  data VARBINARY(max) NOT NULL,
  data_encoding VARCHAR(16) NOT NULL,
  PRIMARY KEY (shard_id)
);

CREATE TABLE executions(
  shard_id INTEGER NOT NULL,
  namespace_id BINARY(16) NOT NULL,
  workflow_id VARCHAR(255) NOT NULL,
  run_id BINARY(16) NOT NULL,
  --
  next_event_id BIGINT NOT NULL,
  last_write_version BIGINT NOT NULL,
  data VARBINARY(max) NOT NULL,
  data_encoding VARCHAR(16) NOT NULL,
  state VARBINARY(max) NOT NULL,
  state_encoding VARCHAR(16) NOT NULL,
  PRIMARY KEY (shard_id, namespace_id, workflow_id, run_id)
);

CREATE TABLE current_executions(
  shard_id INTEGER NOT NULL,
  namespace_id BINARY(16) NOT NULL,
  workflow_id VARCHAR(255) NOT NULL,
  --
  run_id BINARY(16) NOT NULL,
  create_request_id VARCHAR(64) NOT NULL,
  state INTEGER NOT NULL,
  status INTEGER NOT NULL,
  start_version BIGINT NOT NULL,
  last_write_version BIGINT NOT NULL,
  PRIMARY KEY (shard_id, namespace_id, workflow_id)
);

CREATE TABLE buffered_events (
  shard_id INTEGER NOT NULL,
  namespace_id BINARY(16) NOT NULL,
  workflow_id VARCHAR(255) NOT NULL,
  run_id BINARY(16) NOT NULL,
  id BIGINT NOT NULL UNIQUE,
  --
  data VARBINARY(max) NOT NULL,
  data_encoding VARCHAR(16) NOT NULL,
  PRIMARY KEY (shard_id, namespace_id, workflow_id, run_id, id)
);

CREATE TABLE tasks (
  range_hash BIGINT NOT NULL,
  task_queue_id VARBINARY(860) NOT NULL, -- 860 so that it can participate in a primary key of max allowed length of 900
  task_id BIGINT NOT NULL,
  --
  data VARBINARY(max) NOT NULL,
  data_encoding VARCHAR(16) NOT NULL,
  PRIMARY KEY (range_hash, task_queue_id, task_id)
);

CREATE TABLE task_queues (
  range_hash BIGINT NOT NULL,
  task_queue_id VARBINARY(860) NOT NULL, -- 860, see above
  --
  range_id BIGINT NOT NULL,
  data VARBINARY(max) NOT NULL,
  data_encoding VARCHAR(16) NOT NULL,
  PRIMARY KEY (range_hash, task_queue_id)
);

CREATE TABLE transfer_tasks(
  shard_id INTEGER NOT NULL,
  task_id BIGINT NOT NULL,
  --
  data VARBINARY(max) NOT NULL,
  data_encoding VARCHAR(16) NOT NULL,
  PRIMARY KEY (shard_id, task_id)
);

CREATE TABLE timer_tasks (
  shard_id INTEGER NOT NULL,
  visibility_timestamp DATETIME2 NOT NULL,
  task_id BIGINT NOT NULL,
  --
  data VARBINARY(max) NOT NULL,
  data_encoding VARCHAR(16) NOT NULL,
  PRIMARY KEY (shard_id, visibility_timestamp, task_id)
);

CREATE TABLE replication_tasks (
  shard_id INTEGER NOT NULL,
  task_id BIGINT NOT NULL,
  --
  data VARBINARY(max) NOT NULL,
  data_encoding VARCHAR(16) NOT NULL,
  PRIMARY KEY (shard_id, task_id)
);

CREATE TABLE replication_tasks_dlq (
  source_cluster_name VARCHAR(255) NOT NULL,
  shard_id INTEGER NOT NULL,
  task_id BIGINT NOT NULL,
  --
  data VARBINARY(max) NOT NULL,
  data_encoding VARCHAR(16) NOT NULL,
  PRIMARY KEY (source_cluster_name, shard_id, task_id)
);

CREATE TABLE visibility_tasks(
  shard_id INTEGER NOT NULL,
  task_id BIGINT NOT NULL,
  --
  data VARBINARY(max) NOT NULL,
  data_encoding VARCHAR(16) NOT NULL,
  PRIMARY KEY (shard_id, task_id)
);

CREATE TABLE activity_info_maps (
-- each row corresponds to one key of one map<string, ActivityInfo>
  shard_id INTEGER NOT NULL,
  namespace_id BINARY(16) NOT NULL,
  workflow_id VARCHAR(255) NOT NULL,
  run_id BINARY(16) NOT NULL,
  schedule_id BIGINT NOT NULL,
--
  data VARBINARY(max) NOT NULL,
  data_encoding VARCHAR(16),
  PRIMARY KEY (shard_id, namespace_id, workflow_id, run_id, schedule_id)
);

CREATE TABLE timer_info_maps (
  shard_id INTEGER NOT NULL,
  namespace_id BINARY(16) NOT NULL,
  workflow_id VARCHAR(255) NOT NULL,
  run_id BINARY(16) NOT NULL,
  timer_id VARCHAR(255) NOT NULL,
--
  data VARBINARY(max) NOT NULL,
  data_encoding VARCHAR(16),
  PRIMARY KEY (shard_id, namespace_id, workflow_id, run_id, timer_id)
);

CREATE TABLE child_execution_info_maps (
  shard_id INTEGER NOT NULL,
  namespace_id BINARY(16) NOT NULL,
  workflow_id VARCHAR(255) NOT NULL,
  run_id BINARY(16) NOT NULL,
  initiated_id BIGINT NOT NULL,
--
  data VARBINARY(max) NOT NULL,
  data_encoding VARCHAR(16),
  PRIMARY KEY (shard_id, namespace_id, workflow_id, run_id, initiated_id)
);

CREATE TABLE request_cancel_info_maps (
  shard_id INTEGER NOT NULL,
  namespace_id BINARY(16) NOT NULL,
  workflow_id VARCHAR(255) NOT NULL,
  run_id BINARY(16) NOT NULL,
  initiated_id BIGINT NOT NULL,
--
  data VARBINARY(max) NOT NULL,
  data_encoding VARCHAR(16),
  PRIMARY KEY (shard_id, namespace_id, workflow_id, run_id, initiated_id)
);

CREATE TABLE signal_info_maps (
  shard_id INTEGER NOT NULL,
  namespace_id BINARY(16) NOT NULL,
  workflow_id VARCHAR(255) NOT NULL,
  run_id BINARY(16) NOT NULL,
  initiated_id BIGINT NOT NULL,
--
  data VARBINARY(max) NOT NULL,
  data_encoding VARCHAR(16),
  PRIMARY KEY (shard_id, namespace_id, workflow_id, run_id, initiated_id)
);

CREATE TABLE signals_requested_sets (
  shard_id INTEGER NOT NULL,
  namespace_id BINARY(16) NOT NULL,
  workflow_id VARCHAR(255) NOT NULL,
  run_id BINARY(16) NOT NULL,
  signal_id VARCHAR(64) NOT NULL,
  --
  PRIMARY KEY (shard_id, namespace_id, workflow_id, run_id, signal_id)
);

-- history eventsV2: history_node stores history event data
CREATE TABLE history_node (
  shard_id       INTEGER NOT NULL,
  tree_id        BINARY(16) NOT NULL,
  branch_id      BINARY(16) NOT NULL,
  node_id        BIGINT NOT NULL,
  txn_id         BIGINT NOT NULL,
  --
  data           VARBINARY(max) NOT NULL,
  data_encoding  VARCHAR(16) NOT NULL,
  PRIMARY KEY (shard_id, tree_id, branch_id, node_id, txn_id)
);

-- history eventsV2: history_tree stores branch metadata
CREATE TABLE history_tree (
  shard_id       INTEGER NOT NULL,
  tree_id        BINARY(16) NOT NULL,
  branch_id      BINARY(16) NOT NULL,
  --
  data           VARBINARY(max) NOT NULL,
  data_encoding  VARCHAR(16) NOT NULL,
  PRIMARY KEY (shard_id, tree_id, branch_id)
);

CREATE TABLE queue (
  queue_type        INTEGER NOT NULL,
  message_id        BIGINT NOT NULL,
  message_payload   VARBINARY(max) NOT NULL,
  message_encoding  VARCHAR(16) NOT NULL DEFAULT 'Json',
  PRIMARY KEY(queue_type, message_id)
);

CREATE TABLE queue_metadata (
  queue_type     INTEGER NOT NULL,
  data VARBINARY(max)     NOT NULL,
  data_encoding  VARCHAR(16) NOT NULL DEFAULT 'Json',
  PRIMARY KEY(queue_type)
);

CREATE TABLE cluster_metadata (
  metadata_partition        INTEGER NOT NULL,
  data                      VARBINARY(max) NOT NULL,
  data_encoding             VARCHAR(16) NOT NULL,
  version                   BIGINT NOT NULL,
  PRIMARY KEY(metadata_partition)
);

CREATE TABLE cluster_membership
(
    membership_partition INTEGER NOT NULL,
    host_id              BINARY(16) NOT NULL,
    rpc_address          VARCHAR(15) NOT NULL,
    rpc_port             SMALLINT NOT NULL,
    role                 SMALLINT NOT NULL,
    session_start        DATETIME2 DEFAULT '1970-01-01 00:00:01',
    last_heartbeat       DATETIME2 DEFAULT '1970-01-01 00:00:01',
    record_expiry        DATETIME2 DEFAULT '1970-01-01 00:00:01',
    PRIMARY KEY (membership_partition, host_id)
);

------------------------------------------------------------------------------ visibility schema

CREATE TABLE executions_visibility (
  namespace_id         CHAR(64) NOT NULL,
  run_id               CHAR(64) NOT NULL,
  start_time           DATETIME2 NOT NULL,
  execution_time       DATETIME2 NOT NULL,
  workflow_id          VARCHAR(255) NOT NULL,
  workflow_type_name   VARCHAR(255) NOT NULL,
  status               INTEGER NOT NULL,  -- enum WorkflowExecutionStatus {RUNNING, COMPLETED, FAILED, CANCELED, TERMINATED, CONTINUED_AS_NEW, TIMED_OUT}
  close_time           DATETIME2 NULL,
  history_length       BIGINT,
  memo                 VARBINARY(max),
  encoding             VARCHAR(64) NOT NULL,
  task_queue           VARCHAR(255) DEFAULT '' NOT NULL,

  PRIMARY KEY  (namespace_id, run_id)
);

------------------------------------------------------------------------------ end conversion
------------------------------------------------------------------------------ following create index lines adapted manually
CREATE INDEX t_rho on cluster_membership (role, host_id);
CREATE INDEX t_rhb on cluster_membership (role, last_heartbeat);
CREATE INDEX t_ar on cluster_membership (rpc_address, role);
CREATE INDEX t_hb on cluster_membership (last_heartbeat);
CREATE INDEX t_re on cluster_membership (record_expiry);

CREATE INDEX v_type_start_time ON executions_visibility (namespace_id, workflow_type_name, status, start_time DESC, run_id);
CREATE INDEX v_workflow_id_start_time ON executions_visibility (namespace_id, workflow_id, status, start_time DESC, run_id);
CREATE INDEX v_status_by_start_time ON executions_visibility (namespace_id, status, start_time DESC, run_id);
CREATE INDEX v_type_close_time ON executions_visibility (namespace_id, workflow_type_name, status, close_time DESC, run_id);
CREATE INDEX v_workflow_id_close_time ON executions_visibility (namespace_id, workflow_id, status, close_time DESC, run_id);
CREATE INDEX v_status_by_close_time ON executions_visibility (namespace_id, status, close_time DESC, run_id);
CREATE INDEX v_close_time_by_status ON executions_visibility (namespace_id, close_time DESC, run_id, status);
revert;
GO
