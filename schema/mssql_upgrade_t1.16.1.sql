-- 1.5
ALTER TABLE temporal.cluster_membership ALTER COLUMN rpc_address VARCHAR(128);

-- 1.5
ALTER TABLE temporal.history_node ADD prev_txn_id BIGINT NOT NULL DEFAULT 0;

-- 1.5
ALTER TABLE temporal.executions ADD db_record_version BIGINT NOT NULL DEFAULT 0;

-- 1.6
ALTER TABLE temporal.queue_metadata ADD version BIGINT NOT NULL DEFAULT 0;

-- 1.7
CREATE TABLE temporal.cluster_metadata_info (
  metadata_partition        INTEGER NOT NULL,
  cluster_name              VARCHAR(255) NOT NULL,
  data                      VARBINARY(max) NOT NULL,
  data_encoding             VARCHAR(16) NOT NULL,
  version                   BIGINT NOT NULL,
  PRIMARY KEY(metadata_partition, cluster_name)
);

-- 1.7
-- no migration of data from temporal.cluster_metadata to temporal.cluster_metadata_info
-- as this runs into "panic: Version increment 10 is smaller than initial version: map[active:{false 0  0}]."
DROP TABLE temporal.cluster_metadata;

-- 1.7
ALTER TABLE temporal.current_executions ADD CONSTRAINT sver0 DEFAULT 0 FOR start_version;

-- 1.8
ALTER TABLE temporal.current_executions ALTER COLUMN create_request_id VARCHAR(255) NOT NULL;

-- 1.8
ALTER TABLE temporal.signals_requested_sets ALTER COLUMN signal_id VARCHAR(255) NOT NULL;

-- 1.8
UPDATE temporal.schema_version
  SET   curr_version='1.8',    min_compatible_version='1.8'
  WHERE curr_version='1.4' AND min_compatible_version='1.4'
;
