# SQL Scripts

See <https://github.com/temporalio/temporal/tree/master/schema/postgresql> for the
inital postgresql version. We converted the schema with <http://www.sqlines.com/download>.

## Conversion

```cmd
D:\Data\Archiv\sqlines31113\sqlines.exe -s=oracle -t=sql -in=pg\temporal_v96.sql -out=raw\temporal96.sql
D:\Data\Archiv\sqlines31113\sqlines.exe -s=oracle -t=sql -in=pg\visibility_v96.sql -out=raw\visibility96.sql  
```

Plus we converted `BYTEA` to `VARBINARY(max)` if it's data and to `BINARY(16)` if it's an ID.
