package wfping

import (
	"context"
	"fmt"

	"go.temporal.io/sdk/activity"
)

// PingActivity implements a dummy temporal activity
func PingActivity(ctx context.Context, value string) (string, error) {
	l := activity.GetLogger(ctx)
	info := activity.GetInfo(ctx)

	l.Info("ping-activity called", "value", value, "activity-id", info.ActivityID)
	return fmt.Sprintf("activity-value:%v", value), nil
}
