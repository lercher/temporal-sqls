package wfping

import (
	"fmt"
	"time"

	"go.temporal.io/sdk/workflow"
)

const (
	pingWFID              = "ping-workflow-id"
	pingTaskQueueName     = "ping-tq"
	expectedSignalContent = "ping-signal-content"
	signalName            = "sig"
)

// PingWorkflow implements the Ping temporal Workflow
func PingWorkflow(ctx workflow.Context, param string) (string, error) {
	l := workflow.GetLogger(ctx)
	l.Info("ping-workflow started", "param", param)

	// calling an activity
	ao := workflow.ActivityOptions{
		TaskQueue:              pingTaskQueueName,
		ScheduleToStartTimeout: 10 * time.Second,
		StartToCloseTimeout:    5 * time.Second,
		ScheduleToCloseTimeout: 10 * time.Second,
		HeartbeatTimeout:       0,
	}
	actx := workflow.WithActivityOptions(ctx, ao)
	f := workflow.ExecuteActivity(actx, PingActivity, "I'm active with the umlauts äöüßÄÖÜ.")
	var activityResult string
	err := f.Get(ctx, &activityResult)
	if err != nil {
		return "", fmt.Errorf("activity future: %v", err)
	}

	// waiting for a signal
	l.Info("waiting for signal")
	var signal string
	signalChan := workflow.GetSignalChannel(ctx, signalName)
	selector := workflow.NewSelector(ctx)
	selector.AddReceive(signalChan, func(channel workflow.ReceiveChannel, more bool) {
		channel.Receive(ctx, &signal)
		l.Info("received signal", "value", signal)
	})
	selector.Select(ctx)
	if len(signal) > 0 && signal != expectedSignalContent {
		return "", fmt.Errorf("want signal %q value %v, got: %v", signalName, expectedSignalContent, signal)
	}

	msg := fmt.Sprintf("ping-wf %q completed-with: %q and signal value %v", param, activityResult, signal)
	l.Info("completed", "return-value", msg)
	return msg, nil
}
