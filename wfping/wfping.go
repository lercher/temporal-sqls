// Package wfping implements
// a simple temporal workflow with a trivial action
// and some signal processing. It is usefull to test if a temporal.io
// infrastructure service is up and running.
package wfping

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"go.temporal.io/api/workflowservice/v1"
	"go.temporal.io/sdk/client"
	"go.temporal.io/sdk/worker"
)

// Run implements a testing workflow and runs it against the temporal.io server
// at temporaladress. It logs progress to lg.
func Run(lg *log.Logger, temporaladress string) error {
	if !strings.Contains(temporaladress, ":") {
		temporaladress += ":7233"
	}
	lg.Println("Connecting with temporal.io at", temporaladress, "...")

	// The client is a heavyweight temporal object that should be created once
	co := client.Options{
		HostPort: temporaladress,
	}
	serviceClient, err := client.NewClient(co)
	if err != nil {
		return fmt.Errorf("service client: %v", err)
	}
	defer serviceClient.Close()

	nsClient, err := client.NewNamespaceClient(co)
	if err != nil {
		return fmt.Errorf("service namespace client: %v", err)
	}
	defer nsClient.Close()

	retentionPeriod30days := time.Hour * 24 * 30
	err = nsClient.Register(context.Background(), &workflowservice.RegisterNamespaceRequest{
		Namespace:                        "default",
		WorkflowExecutionRetentionPeriod: &retentionPeriod30days,
	})
	if err != nil {
		lg.Println("registering namespace default:", err.Error())
	} else {
		lg.Println("namespace default: registered (waiting 1s) ...")
		<-time.After(time.Second)
	}

	lg.Println("new worker, registering wf and activity ...")
	w := worker.New(serviceClient, pingTaskQueueName, worker.Options{})
	w.RegisterWorkflow(PingWorkflow)
	w.RegisterActivity(PingActivity)
	err = w.Start()
	if err != nil {
		return fmt.Errorf("worker start: %v", err)
	}
	defer w.Stop()

	lg.Println("Starting the ping WF ...")
	workflowOptions := client.StartWorkflowOptions{
		TaskQueue:          pingTaskQueueName,
		ID:                 pingWFID,
		WorkflowRunTimeout: time.Second * 10,
	}
	param := time.Now().Format(time.RFC3339)
	workflowRun, err := serviceClient.ExecuteWorkflow(context.Background(), workflowOptions, PingWorkflow, param)
	if err != nil {
		return fmt.Errorf("execute wf: %v", err)
	}

	lg.Println("Waiting a second to send the signal ...")
	<-time.After(time.Second)
	lg.Println("Sending the signal ..")
	err = sendSignal(serviceClient)
	if err != nil {
		return err
	}

	lg.Println("Get WF result (max 5s) ...")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()
	var result string
	err = workflowRun.Get(ctx, &result)
	if err != nil {
		return fmt.Errorf("workflowRun result: %v", err)
	}

	lg.Println("WF run OK, result:", result)
	return nil
}

func sendSignal(cli client.Client) error {
	err := cli.SignalWorkflow(context.Background(), pingWFID, "", signalName, expectedSignalContent)
	if err != nil {
		return fmt.Errorf("Error sending the signal: %v", err)
	}
	return nil
}
