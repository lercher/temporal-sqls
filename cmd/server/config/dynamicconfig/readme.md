# dynamicconfig

Configuration files in this folder are evaluated during runtime.

The polling interval is specified in the YAML file in this
folder's parent folder under `dynamicConfigClient/pollInterval`
and because e.g.
`dynamicConfigClient/filepath: "./config/dynamicconfig/development.yaml"`
is specified there.

Relevant snippet from [../development.yaml](../development.yaml)

```yaml
dynamicConfigClient:
  filepath: "./config/dynamicconfig/development.yaml"
  pollInterval: "30s"
```
