package main

import (
	"log"
	"os"

	"gitlab.com/lercher/temporal-sqls/wfping"
)

func main() {
	log.Println("This is WFPing. (C) 2022 by Martin Lercher.")
	log.Println()
	log.Println("It hosts and runs a simple temporal workflow with a trivial action")
	log.Println("and some signal processing. It is usefull to test if a temporal.io")
	log.Println("infrastructure service is up and running.")
	log.Println()
	log.Println("usage: wfping [temporal-frontend-service-host[:port]]")
	log.Println()

	t := "localhost:7233"
	if len(os.Args) == 2 {
		t = os.Args[1]
	}

	err := wfping.Run(log.Default(), t)
	if err != nil {
		log.Fatal(err)
	}
	log.Println("================= DONE sucessfully =================")
}
