// The MIT License
//
// Copyright (c) 2021-2022 Martin Lercher.  All rights reserved.
//
// Copyright (c) 2020 Temporal Technologies Inc.  All rights reserved.
//
// Copyright (c) 2020 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package sqlserver

import (
	"context"
	gosql "database/sql"
	"errors"
	"fmt"
	"log"
	"strings"
	"sync"
	"time"

	mssql "github.com/denisenkom/go-mssqldb"
	"gitlab.com/lercher/temporal-sqls/instrumentedsql"

	"github.com/iancoleman/strcase"
	"github.com/jmoiron/sqlx"

	"go.temporal.io/server/common/config"
	"go.temporal.io/server/common/persistence/sql"
	"go.temporal.io/server/common/persistence/sql/sqlplugin"
	"go.temporal.io/server/common/resolver"
)

const (
	// PluginName is the name of the plugin
	// and it is used for sql.Connect to specify the driver name
	PluginName                = "sqlserver"
	driverNameInstrumented    = "sqlserver-i" // must not be sqlserver, b/c of "panic: sql: Register called twice for driver sqlserver"
	driverNameNotInstrumented = "sqlserver"   // must be sqlserver or else sqlx won't replace named params with @name and keep `?`
)

var errTLSNotImplemented = errors.New("tls for " + PluginName + " has not been implemented")

// Plugin type to be uesd from external tests
type Plugin struct{}

var _ sqlplugin.Plugin = (*Plugin)(nil)

func init() {
	sql.RegisterPlugin(PluginName, &Plugin{})
}

var (
	dsnMapName = []string{ // indexed by sqlplugin.DbKind 1/2, spaces for formatting
		"unknown-db ", // sqlplugin.DbKindUnknown - unused
		"main-db    ", // sqlplugin.DbKindMain
		"standard-db", // sqlplugin.DbKindVisibility
	}
	dsnMap     = make([]string, 3) // indexed by sqlplugin.DbKind 1/2
	dsnLog     = make([]bool, 3)   // indexed by sqlplugin.DbKind 1/2
	dsnMapLock sync.Mutex
	once       = sync.Once{}
)

// CreateDB initialize the db object
func (d *Plugin) CreateDB(
	dbKind sqlplugin.DbKind,
	cfg *config.SQL,
	r resolver.ServiceResolver,
) (sqlplugin.DB, error) {

	conn, err := d.createDBConnection(dbKind, cfg, r)
	if err != nil {
		return nil, err
	}
	db := newDB(dbKind, cfg.DatabaseName, conn, nil)
	return db, nil
}

// CreateAdminDB initialize the adminDB object
func (d *Plugin) CreateAdminDB(
	dbKind sqlplugin.DbKind,
	cfg *config.SQL,
	r resolver.ServiceResolver,
) (sqlplugin.AdminDB, error) {

	conn, err := d.createDBConnection(dbKind, cfg, r)
	if err != nil {
		return nil, err
	}
	db := newDB(dbKind, cfg.DatabaseName, conn, nil)
	return db, nil
}

// CreateDBConnection creates a returns a reference to a logical connection to the
// underlying SQL database. The returned object is to tied to a single
// SQL database and the object can be used to perform CRUD operations on
// the tables in the database
func (d *Plugin) createDBConnection(
	dbKind sqlplugin.DbKind,
	cfg *config.SQL,
	r resolver.ServiceResolver,
) (*sqlx.DB, error) {

	once.Do(func() {
		log.Println("see https://github.com/denisenkom/go-mssqldb#connection-parameters-and-dsn for additional options")
		log.Println("  in <env>.yaml at persistence/datastores/<sqlserver-default>/sql/connectAttributes")
		log.Println("  where logsql: true/false under connectAttributes enables or disables SQL logging")

		driver := instrumentedsql.WrapDriver(&mssql.Driver{}) // adds logging and converts some temporal types to basic types the sql server driver understands
		gosql.Register(driverNameInstrumented, driver)
	})

	dsn, logsql, logVersion := dsnMap[dbKind], dsnLog[dbKind], false
	if dsn == "" {
		dsn, logsql = buildDSN(dbKind, cfg, r)
		dsnMapLock.Lock() // just lock the writes it doesn't matter, if we create a dsn several extra times during start, finally it will be cached
		dsnMap[dbKind] = dsn
		dsnLog[dbKind] = logsql		
		dsnMapLock.Unlock()
		logVersion = true
	}

	dn := driverNameNotInstrumented
	if logsql {
		dn = driverNameInstrumented
	}

	inner, err := gosql.Open(dn, dsn)
	if err != nil {
		return nil, err
	}

	db := sqlx.NewDb(inner, driverNameNotInstrumented)
	if cfg.MaxConns > 0 {
		db.SetMaxOpenConns(cfg.MaxConns)
	}
	if cfg.MaxIdleConns > 0 {
		db.SetMaxIdleConns(cfg.MaxIdleConns)
	}
	if cfg.MaxConnLifetime > 0 {
		db.SetConnMaxLifetime(cfg.MaxConnLifetime)
	}

	if logVersion {
		err = logServerVersion(inner)
		if err != nil {
			log.Println("logServerVersion:", err)
		}
	}

	// Maps struct names in CamelCase to snake without need for db struct tags.
	db.MapperFunc(strcase.ToSnake)
	return db, nil
}

func logServerVersion(db *gosql.DB) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second * 5)
	defer cancel()

	row := db.QueryRowContext(ctx, "select @@VERSION as v")
	var v string
	err := row.Scan(&v)
	if err != nil {
		return err
	}
	log.Println("sqlserver version:", v)
	return nil
}

func buildDSN(
	dbKind sqlplugin.DbKind,
	cfg *config.SQL,
	r resolver.ServiceResolver,
) (string, bool) {

	server := r.Resolve(cfg.ConnectAddr)[0]
	if idxColon := strings.Index(server, ":"); idxColon >= 0 {
		// remove :port trailer from server name. The test suite adds it without need.
		server = server[:idxColon]
	}

	encryptEqualsTrue := ""
	if cfg.TLS != nil && cfg.TLS.Enabled {
		encryptEqualsTrue = "encrypt=true;"
	}

	if cfg.DatabaseName == "" {
		panic(`cfg.DatabaseName is empty, if test suite, see github.com\temporalio\temporal\common\persistence\sql\sqlPersistenceTest.go to be: if cfg2.PluginName != "sqlite" && cfg2.PluginName != "sqlserver" {`)
	}
	dsn := fmt.Sprintf("server=%s;database=%s;app name=%s;%v",
		server,
		cfg.DatabaseName,
		"temporal.io",
		encryptEqualsTrue, // full "encrypt=true;" or else empty
	)

	logsql := false
	for k, v := range cfg.ConnectAttributes {
		switch k {
		case "logsql":
			v := strings.ToLower(v)
			logsql = (v == "t" || v == "true" || v == "1" || v == "yes")
		default:
			dsn += fmt.Sprintf("%s=%q;", k, v)
		}
	}

	if cfg.User == "" || cfg.User == "*" {
		if cfg.Password == "logsql=true" { // used as hack in plugin_test.go to turn on logging
			logsql = true
		}
		// use sql server trusted (aka integrated) connection, i.e. connect as the user running the process
		dsn += "trusted_connection=yes;"
		log.Println("connecting to", cfg.PluginName, dsnMapName[dbKind], dsn, "logging:", logsql)
	} else {
		// use user/password to connect
		log.Println("connecting to", cfg.PluginName, dsnMapName[dbKind], dsn, "as user", cfg.User, "with a hidden password", "logging:", logsql)
		dsn += fmt.Sprintf("user id=%s;password=%s;",
			cfg.User,
			cfg.Password,
		)
	}

	return dsn, logsql
}
