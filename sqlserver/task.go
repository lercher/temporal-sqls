// The MIT License
//
// Copyright (c) 2021-2022 Martin Lercher.  All rights reserved.
//
// Copyright (c) 2020 Temporal Technologies Inc.  All rights reserved.
//
// Copyright (c) 2020 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package sqlserver

import (
	"context"
	"database/sql"
	"fmt"

	"go.temporal.io/api/serviceerror"

	"go.temporal.io/server/common/persistence/sql/sqlplugin"
)

const (
	// Task_Queues Table
	// (default range ID: initialRangeID == 1)
	createTaskQueueQry = `INSERT INTO temporal.task_queues (range_hash, task_queue_id, range_id, data, data_encoding) VALUES (:range_hash, :task_queue_id, :range_id, :data, :data_encoding)`
	updateTaskQueueQry = `UPDATE temporal.task_queues SET range_id = :range_id, data = :data, data_encoding = :data_encoding WHERE range_hash = :range_hash AND task_queue_id = :task_queue_id`
	lockTaskQueueQry   = `SELECT range_id FROM temporal.task_queues WITH (UPDLOCK,READPAST,ROWLOCK) WHERE range_hash=@p1 AND task_queue_id=@p2` // FOR UPDATE
	deleteTaskQueueQry = `DELETE FROM temporal.task_queues WHERE range_hash=@p1 AND task_queue_id=@p2 AND range_id=@p3`

	listTaskQueueRowSelectPart    = ` range_hash, task_queue_id, range_id, data, data_encoding from temporal.task_queues ` // sth + . + sth
	listTaskQueueWithHashRangeQry = `SELECT TOP (@p4)` + listTaskQueueRowSelectPart + `WHERE range_hash between @p1 AND @p2 AND task_queue_id>@p3 ORDER BY task_queue_id ASC`
	listTaskQueueQry              = `SELECT TOP (@p3)` + listTaskQueueRowSelectPart + `WHERE range_hash=@p1 AND task_queue_id>@p2 ORDER BY task_queue_id ASC`
	getTaskQueueQry               = `SELECT` + listTaskQueueRowSelectPart + `WHERE range_hash=@p1 AND task_queue_id=@p2`
)

const (
	// Tasks Table
	getTaskMinMaxQry = `SELECT TOP (@p5) task_id, data, data_encoding FROM temporal.tasks WHERE range_hash = @p1 AND task_queue_id=@p2 AND @p3<=task_id AND task_id<@p4 ORDER BY task_id`
	getTaskMinQry    = `SELECT TOP (@p4) task_id, data, data_encoding FROM temporal.tasks WHERE range_hash = @p1 AND task_queue_id=@p2 AND @p3<=task_id ORDER BY task_id`
	createTaskQry    = `INSERT INTO temporal.tasks (range_hash, task_queue_id, task_id, data, data_encoding) VALUES (:range_hash, :task_queue_id, :task_id, :data, :data_encoding)`
	deleteTaskQry    = `DELETE FROM temporal.tasks WHERE range_hash=@p1 AND task_queue_id=@p2 AND task_id=@p3`

	rangeDeleteTaskQry = `DELETE FROM temporal.tasks WHERE range_hash=@p1 AND task_queue_id=@p2 AND task_id IN (SELECT TOP (@p4) task_id FROM temporal.tasks WHERE range_hash=@p1 AND task_queue_id=@p2 AND task_id<@p3 ORDER BY task_queue_id,task_id)`
)

// InsertIntoTasks inserts one or more rows into tasks table
func (pdb *db) InsertIntoTasks(
	ctx context.Context,
	rows []sqlplugin.TasksRow,
) (sql.Result, error) {
	return pdb.conn.NamedExecContext(ctx,
		createTaskQry,
		rows,
	)
}

// SelectFromTasks reads one or more rows from tasks table
func (pdb *db) SelectFromTasks(
	ctx context.Context,
	filter sqlplugin.TasksFilter,
) ([]sqlplugin.TasksRow, error) {
	var err error
	var rows []sqlplugin.TasksRow
	switch {
	case filter.ExclusiveMaxTaskID != nil:
		err = pdb.conn.SelectContext(ctx,
			&rows,
			getTaskMinMaxQry,
			filter.RangeHash,
			filter.TaskQueueID,
			*filter.InclusiveMinTaskID,
			*filter.ExclusiveMaxTaskID,
			*filter.PageSize,
		)
	default:
		err = pdb.conn.SelectContext(ctx,
			&rows,
			getTaskMinQry,
			filter.RangeHash,
			filter.TaskQueueID,
			*filter.InclusiveMinTaskID,
			*filter.PageSize,
		)
	}
	return rows, err
}

// DeleteFromTasks deletes one or more rows from tasks table
func (pdb *db) DeleteFromTasks(
	ctx context.Context,
	filter sqlplugin.TasksFilter,
) (sql.Result, error) {
	if filter.ExclusiveMaxTaskID != nil {
		if filter.Limit == nil || *filter.Limit == 0 {
			return nil, fmt.Errorf("missing limit parameter")
		}
		return pdb.conn.ExecContext(ctx,
			rangeDeleteTaskQry,
			filter.RangeHash,
			filter.TaskQueueID,
			*filter.ExclusiveMaxTaskID,
			*filter.Limit,
		)
	}
	return pdb.conn.ExecContext(ctx,
		deleteTaskQry,
		filter.RangeHash,
		filter.TaskQueueID,
		*filter.TaskID,
	)
}

// InsertIntoTaskQueues inserts one or more rows into task_queues table
func (pdb *db) InsertIntoTaskQueues(
	ctx context.Context,
	row *sqlplugin.TaskQueuesRow,
) (sql.Result, error) {
	return pdb.conn.NamedExecContext(ctx,
		createTaskQueueQry,
		row,
	)
}

// UpdateTaskQueues updates a row in task_queues table
func (pdb *db) UpdateTaskQueues(
	ctx context.Context,
	row *sqlplugin.TaskQueuesRow,
) (sql.Result, error) {
	return pdb.conn.NamedExecContext(ctx,
		updateTaskQueueQry,
		row,
	)
}

// SelectFromTaskQueues reads one or more rows from task_queues table
func (pdb *db) SelectFromTaskQueues(
	ctx context.Context,
	filter sqlplugin.TaskQueuesFilter,
) ([]sqlplugin.TaskQueuesRow, error) {
	switch {
	case filter.TaskQueueID != nil:
		if filter.RangeHashLessThanEqualTo != 0 || filter.RangeHashGreaterThanEqualTo != 0 {
			return nil, serviceerror.NewInternal("shardID range not supported for specific selection")
		}
		return pdb.selectFromTaskQueues(ctx, filter)
	case filter.RangeHashLessThanEqualTo != 0 && filter.PageSize != nil:
		if filter.RangeHashLessThanEqualTo < filter.RangeHashGreaterThanEqualTo {
			return nil, serviceerror.NewInternal("range of hashes bound is invalid")
		}
		return pdb.rangeSelectFromTaskQueues(ctx, filter)
	case filter.TaskQueueIDGreaterThan != nil && filter.PageSize != nil:
		return pdb.rangeSelectFromTaskQueues(ctx, filter)
	default:
		return nil, serviceerror.NewInternal("invalid set of query filter params")
	}
}

func (pdb *db) selectFromTaskQueues(
	ctx context.Context,
	filter sqlplugin.TaskQueuesFilter,
) ([]sqlplugin.TaskQueuesRow, error) {
	var err error
	var row sqlplugin.TaskQueuesRow
	err = pdb.conn.GetContext(ctx,
		&row,
		getTaskQueueQry,
		filter.RangeHash,
		filter.TaskQueueID,
	)
	if err != nil {
		return nil, err
	}
	return []sqlplugin.TaskQueuesRow{row}, err
}

func (pdb *db) rangeSelectFromTaskQueues(
	ctx context.Context,
	filter sqlplugin.TaskQueuesFilter,
) ([]sqlplugin.TaskQueuesRow, error) {
	var err error
	var rows []sqlplugin.TaskQueuesRow
	if filter.RangeHashLessThanEqualTo > 0 {
		err = pdb.conn.SelectContext(ctx,
			&rows,
			listTaskQueueWithHashRangeQry,
			filter.RangeHashGreaterThanEqualTo,
			filter.RangeHashLessThanEqualTo,
			filter.TaskQueueIDGreaterThan,
			*filter.PageSize,
		)
	} else {
		err = pdb.conn.SelectContext(ctx,
			&rows,
			listTaskQueueQry,
			filter.RangeHash,
			filter.TaskQueueIDGreaterThan,
			*filter.PageSize,
		)
	}
	if err != nil {
		return nil, err
	}

	return rows, nil
}

// DeleteFromTaskQueues deletes a row from task_queues table
func (pdb *db) DeleteFromTaskQueues(
	ctx context.Context,
	filter sqlplugin.TaskQueuesFilter,
) (sql.Result, error) {
	return pdb.conn.ExecContext(ctx,
		deleteTaskQueueQry,
		filter.RangeHash,
		filter.TaskQueueID,
		*filter.RangeID,
	)
}

// LockTaskQueues locks a row in task_queues table
func (pdb *db) LockTaskQueues(
	ctx context.Context,
	filter sqlplugin.TaskQueuesFilter,
) (int64, error) {
	var rangeID int64
	err := pdb.conn.GetContext(ctx,
		&rangeID,
		lockTaskQueueQry,
		filter.RangeHash,
		filter.TaskQueueID,
	)
	return rangeID, err
}
