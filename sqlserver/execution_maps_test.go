package sqlserver_test

import (
	"context"
	"testing"
	"time"

	"gitlab.com/lercher/temporal-sqls/sqlserver"

	"go.temporal.io/server/common/config"
	"go.temporal.io/server/common/persistence/sql/sqlplugin"
)

type mockresolver int

func (m mockresolver) Resolve(service string) []string {
	return []string{service}
}

// v1.7    passes
// v1.16.1 passes
func Test_db_DeleteFromRequestCancelInfoMaps(t *testing.T) {
	p := sqlserver.Plugin{}
	pdb, err := p.CreateDB(
		sqlplugin.DbKindMain,
		&config.SQL{
			DatabaseName:      testTemporalDBName,
			ConnectAddr:       "localhost",
			MaxConns:          20,
			MaxIdleConns:      20,
			MaxConnLifetime:   time.Hour,
			ConnectAttributes: map[string]string{"logsql": "false"}, // "true" to log all mssql server statements
		},
		mockresolver(0),
	)

	if err != nil {
		t.Fatal(err)
	}
	defer pdb.Close()

	ctx := context.Background()
	filter := sqlplugin.RequestCancelInfoMapsFilter{
		ShardID:      -998877, // just a random non-existing shardID
		NamespaceID:  []byte("(no-namespace)"),
		WorkflowID:   "(no-wf)",
		RunID:        []byte{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
		InitiatedIDs: []int64{-10, -20, -30, -40},
	}

	// DELETE FROM temporal.request_cancel_info_maps WHERE shard_id=@p1 AND namespace_id=@p2 AND workflow_id=@p3 AND run_id=@p4 AND initiated_id IN (@p5, @p6, @p7, @p8)
	// [{ 1 -998877} { 2 [40 110 111 45 110 97 109 101 115 112 97 99 101 41]} { 3 (no-wf)} { 4 [0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15]} { 5 -10} { 6 -20} { 7 -30} { 8 -40}]
	got, err := pdb.DeleteFromRequestCancelInfoMaps(ctx, filter)
	if err != nil {
		t.Fatal(err)
	}
	ra, err := got.RowsAffected()
	if err != nil {
		t.Fatal(err)
	}
	if ra != 0 {
		t.Errorf("RowsAffected: want 0, got %d", ra)
	}
}
