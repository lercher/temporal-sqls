// The MIT License
//
// Copyright (c) 2021-2022 Martin Lercher.  All rights reserved.
//
// Copyright (c) 2020 Temporal Technologies Inc.  All rights reserved.
//
// Copyright (c) 2020 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package sqlserver

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"strings"

	"go.temporal.io/server/common/persistence/sql/sqlplugin"
)

const (
	templateCreateWorkflowExecutionStarted = `INSERT INTO temporal.executions_visibility (namespace_id, workflow_id, run_id, start_time, execution_time, workflow_type_name, status, memo, encoding, task_queue) VALUES (@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8,@p9,@p10)` // ON CONFLICT (namespace_id, run_id) DO NOTHING

	templateCreateWorkflowExecutionClosed = `BEGIN TRAN
UPDATE temporal.executions_visibility
SET workflow_id = @p2,
	start_time = @p4,
	execution_time = @p5,
	workflow_type_name = @p6,
	close_time = @p7,
	status = @p8,
	history_length = @p9,
	memo = @p10,
	encoding = @p11,
	task_queue = @p12
WHERE namespace_id=@p1 AND run_id=@p3
IF @@ROWCOUNT=0 
	INSERT INTO temporal.executions_visibility (namespace_id, workflow_id, run_id, start_time, execution_time, workflow_type_name, close_time, status, history_length, memo, encoding, task_queue) 
	VALUES (@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8,@p9,@p10,@p11,@p12)
COMMIT`

	// ORDER BY RunID is needed for correct pagination

	templateConditionsOpenxxWorkflow1top7 = ` AND namespace_id=@p1
  AND start_time between @p2 AND @p3
  AND ((run_id>@p4 and start_time=@p5) OR (start_time<@p6))
ORDER BY start_time DESC, run_id`

	templateConditionsOpenxxWorkflow2top8 = ` AND namespace_id=@p2
  AND start_time between @p3 AND @p4
  AND ((run_id>@p5 and start_time=@p6) OR (start_time<@p7))
ORDER BY start_time DESC, run_id`

	templateConditionsClosedWorkflow1top7 = ` AND namespace_id=@p1
  AND start_time between @p2 AND @p3
  AND ((run_id>@p4 and close_time=@p5) OR (close_time<@p6))
ORDER BY close_time DESC, run_id`

	templateConditionsClosedWorkflow2top8 = ` AND namespace_id=@p2
  AND start_time between @p3 AND @p4
  AND ((run_id>@p5 and close_time=@p6) OR (close_time<@p7))
ORDER BY close_time DESC, run_id`

	selectTop7       = `SELECT TOP (@p7)`
	selectTop8       = `SELECT TOP (@p8)`
	openxxExecutions = ` workflow_id, run_id, start_time, execution_time, workflow_type_name, status, memo, encoding, task_queue                             FROM temporal.executions_visibility WHERE status =1 `
	closedExecutions = ` workflow_id, run_id, start_time, execution_time, workflow_type_name, status, memo, encoding, task_queue, close_time, history_length FROM temporal.executions_visibility WHERE status!=1 `

	templateGetOpenWorkflowExecutions           = selectTop7 + openxxExecutions + templateConditionsOpenxxWorkflow1top7
	templateGetClosedWorkflowExecutions         = selectTop7 + closedExecutions + templateConditionsClosedWorkflow1top7
	templateGetOpenWorkflowExecutionsByType     = selectTop8 + openxxExecutions + `AND workflow_type_name=@p1` + templateConditionsOpenxxWorkflow2top8
	templateGetClosedWorkflowExecutionsByType   = selectTop8 + closedExecutions + `AND workflow_type_name=@p1` + templateConditionsClosedWorkflow2top8
	templateGetOpenWorkflowExecutionsByID       = selectTop8 + openxxExecutions + `AND workflow_id=@p1` + templateConditionsOpenxxWorkflow2top8
	templateGetClosedWorkflowExecutionsByID     = selectTop8 + closedExecutions + `AND workflow_id=@p1` + templateConditionsClosedWorkflow2top8
	templateGetClosedWorkflowExecutionsByStatus = selectTop8 + closedExecutions + `AND status=@p1` + templateConditionsClosedWorkflow2top8

	templateGetClosedWorkflowExecution = `SELECT workflow_id, run_id, start_time, execution_time, memo, encoding, close_time, workflow_type_name, status, history_length FROM temporal.executions_visibility WHERE namespace_id=@p1 AND status!=1 AND run_id=@p2`

	templateDeleteWorkflowExecution = "DELETE FROM temporal.executions_visibility WHERE namespace_id=@p1 AND run_id=@p2"
)

var errCloseParams = errors.New("missing one of {closeTime, historyLength} params")

// InsertIntoVisibility inserts a row into visibility table. If an row already exist,
// its left as such and no update will be made
func (pdb *db) InsertIntoVisibility(
	ctx context.Context,
	row *sqlplugin.VisibilityRow,
) (sql.Result, error) {
	row.StartTime = pdb.converter.FromServerDateTime(row.StartTime)
	r, err := pdb.conn.ExecContext(ctx,
		templateCreateWorkflowExecutionStarted,
		row.NamespaceID,
		row.WorkflowID,
		row.RunID,
		row.StartTime,
		row.ExecutionTime,
		row.WorkflowTypeName,
		row.Status,
		row.Memo,
		row.Encoding,
		row.TaskQueue,
	)
	if isDupEntryError(err) {
		// ON CONFLICT DO NOTHING for templateCreateWorkflowExecutionStarted
		return r, nil
	}
	return r, err
}

// ReplaceIntoVisibility replaces an existing row if it exist or creates a new row in visibility table
func (pdb *db) ReplaceIntoVisibility(
	ctx context.Context,
	row *sqlplugin.VisibilityRow,
) (sql.Result, error) {
	switch {
	case row.CloseTime != nil && row.HistoryLength != nil:
		row.StartTime = pdb.converter.ToServerDateTime(row.StartTime)
		closeTime := pdb.converter.ToServerDateTime(*row.CloseTime)
		return pdb.conn.ExecContext(ctx,
			templateCreateWorkflowExecutionClosed,
			row.NamespaceID,
			row.WorkflowID,
			row.RunID,
			row.StartTime,
			row.ExecutionTime,
			row.WorkflowTypeName,
			closeTime,
			row.Status,
			*row.HistoryLength,
			row.Memo,
			row.Encoding,
			row.TaskQueue,
		)
	default:
		return nil, errCloseParams
	}
}

// DeleteFromVisibility deletes a row from visibility table if it exist
func (pdb *db) DeleteFromVisibility(
	ctx context.Context,
	filter sqlplugin.VisibilityDeleteFilter,
) (sql.Result, error) {
	return pdb.conn.ExecContext(ctx,
		templateDeleteWorkflowExecution,
		filter.NamespaceID,
		filter.RunID,
	)
}

// SelectFromVisibility reads one or more rows from visibility table
func (pdb *db) SelectFromVisibility(
	ctx context.Context,
	filter sqlplugin.VisibilitySelectFilter,
) ([]sqlplugin.VisibilityRow, error) {
	var err error
	var rows []sqlplugin.VisibilityRow
	if filter.MinTime != nil {
		*filter.MinTime = pdb.converter.ToServerDateTime(*filter.MinTime)
	}
	if filter.MaxTime != nil {
		*filter.MaxTime = pdb.converter.ToServerDateTime(*filter.MaxTime)
	}
	// If filter.Status == 0 (UNSPECIFIED) then only closed workflows will be returned (all excluding 1 (RUNNING)).
	switch {
	case filter.MinTime == nil && filter.RunID != nil && filter.Status != 1:
		var row sqlplugin.VisibilityRow
		err = pdb.conn.GetContext(ctx,
			&row,
			templateGetClosedWorkflowExecution,
			filter.NamespaceID,
			*filter.RunID,
		)
		if err == nil {
			rows = append(rows, row)
		}
	case filter.MinTime != nil && filter.MaxTime != nil &&
		filter.WorkflowID != nil && filter.RunID != nil && filter.PageSize != nil:
		qry := templateGetOpenWorkflowExecutionsByID
		if filter.Status != 1 {
			qry = templateGetClosedWorkflowExecutionsByID
		}
		err = pdb.conn.SelectContext(ctx,
			&rows,
			qry,
			*filter.WorkflowID,
			filter.NamespaceID,
			*filter.MinTime,
			*filter.MaxTime,
			*filter.RunID,
			*filter.MaxTime,
			*filter.MaxTime,
			*filter.PageSize)
	case filter.MinTime != nil && filter.MaxTime != nil &&
		filter.WorkflowTypeName != nil && filter.RunID != nil && filter.PageSize != nil:
		qry := templateGetOpenWorkflowExecutionsByType
		if filter.Status != 1 {
			qry = templateGetClosedWorkflowExecutionsByType
		}
		err = pdb.conn.SelectContext(ctx,
			&rows,
			qry,
			*filter.WorkflowTypeName,
			filter.NamespaceID,
			*filter.MinTime,
			*filter.MaxTime,
			*filter.RunID,
			*filter.MaxTime,
			*filter.MaxTime,
			*filter.PageSize)
	case filter.MinTime != nil && filter.MaxTime != nil &&
		filter.RunID != nil && filter.PageSize != nil &&
		filter.Status != 0 && filter.Status != 1: // 0 is UNSPECIFIED, 1 is RUNNING
		err = pdb.conn.SelectContext(ctx,
			&rows,
			templateGetClosedWorkflowExecutionsByStatus,
			filter.Status,
			filter.NamespaceID,
			*filter.MinTime,
			*filter.MaxTime,
			*filter.RunID,
			*filter.MaxTime,
			*filter.MaxTime,
			*filter.PageSize)
	case filter.MinTime != nil && filter.MaxTime != nil &&
		filter.RunID != nil && filter.PageSize != nil:
		qry := templateGetOpenWorkflowExecutions
		if filter.Status != 1 {
			qry = templateGetClosedWorkflowExecutions
		}
		err = pdb.conn.SelectContext(ctx,
			&rows,
			qry,
			filter.NamespaceID,
			*filter.MinTime,
			*filter.MaxTime,
			*filter.RunID,
			*filter.MaxTime,
			*filter.MaxTime,
			*filter.PageSize)
	default:
		return nil, fmt.Errorf("invalid query filter")
	}
	if err != nil {
		return nil, err
	}
	for i := range rows {
		rows[i].StartTime = pdb.converter.FromServerDateTime(rows[i].StartTime)
		rows[i].ExecutionTime = pdb.converter.FromServerDateTime(rows[i].ExecutionTime)
		if rows[i].CloseTime != nil {
			closeTime := pdb.converter.FromServerDateTime(*rows[i].CloseTime)
			rows[i].CloseTime = &closeTime
		}
		// need to trim the run ID, or otherwise the returned value will
		//  come with lots of trailing spaces, probably due to the CHAR(64) type
		rows[i].RunID = strings.TrimSpace(rows[i].RunID)
	}
	return rows, nil
}
