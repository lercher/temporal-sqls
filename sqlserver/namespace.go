// The MIT License
//
// Copyright (c) 2021-2022 Martin Lercher.  All rights reserved.
//
// Copyright (c) 2020 Temporal Technologies Inc.  All rights reserved.
//
// Copyright (c) 2020 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package sqlserver

import (
	"context"
	"database/sql"
	"errors"

	"go.temporal.io/api/serviceerror"

	"go.temporal.io/server/common/persistence/sql/sqlplugin"
)

const (
	createNamespaceQuery = `INSERT INTO temporal.namespaces (partition_id, id, name, is_global, data, data_encoding, notification_version) VALUES (@p1,@p2,@p3,@p4,@p5,@p6,@p7)`

	updateNamespaceQuery = `UPDATE temporal.namespaces SET name=@p1, data=@p2, data_encoding=@p3, is_global=@p4, notification_version=@p5 WHERE partition_id=` + namespaceMetadataSingletonPartitionIDString + ` AND id=@p6`

	getNamespacePart = ` id, name, is_global, data, data_encoding, notification_version FROM temporal.namespaces`

	getNamespaceByIDQuery    = `SELECT` + getNamespacePart + ` WHERE partition_id=@p1 AND id=@p2`
	getNamespaceByNameQuery  = `SELECT` + getNamespacePart + ` WHERE partition_id=@p1 AND name=@p2`
	listNamespacesQuery      = `SELECT TOP (@p2)` + getNamespacePart + ` WHERE partition_id=@p1 ORDER BY id`
	listNamespacesRangeQuery = `SELECT TOP (@p3)` + getNamespacePart + ` WHERE partition_id=@p1 AND id>@p2 ORDER BY id`

	deleteNamespaceByIDQuery   = `DELETE FROM temporal.namespaces WHERE partition_id=@p1 AND id=@p2`
	deleteNamespaceByNameQuery = `DELETE FROM temporal.namespaces WHERE partition_id=@p1 AND name=@p2`

	getNamespaceMetadataQuery    = `SELECT notification_version FROM temporal.namespace_metadata WHERE partition_id=@p1`
	lockNamespaceMetadataQuery   = `SELECT notification_version FROM temporal.namespace_metadata WITH (UPDLOCK,READPAST) WHERE partition_id=@p1` // FOR UPDATE
	updateNamespaceMetadataQuery = `UPDATE temporal.namespace_metadata SET notification_version=@p1 WHERE notification_version=@p2 AND partition_id=@p3`
)

const (
	// 54321 is the PK ID of a singleton record in table namespace_metadata. This record must exist. It is for lcoking only
	namespaceMetadataSingletonPartitionID       int64 = 54321
	namespaceMetadataSingletonPartitionIDString       = `54321`
)

var errMissingArgs = errors.New("missing one or more args for API")

// InsertIntoNamespace inserts a single row into namespaces table
func (pdb *db) InsertIntoNamespace(
	ctx context.Context,
	row *sqlplugin.NamespaceRow,
) (sql.Result, error) {
	return pdb.conn.ExecContext(ctx, createNamespaceQuery, namespaceMetadataSingletonPartitionID, []byte(row.ID), row.Name, row.IsGlobal, row.Data, row.DataEncoding, row.NotificationVersion)
}

// UpdateNamespace updates a single row in namespaces table
func (pdb *db) UpdateNamespace(
	ctx context.Context,
	row *sqlplugin.NamespaceRow,
) (sql.Result, error) {
	return pdb.conn.ExecContext(ctx, updateNamespaceQuery, row.Name, row.Data, row.DataEncoding, row.IsGlobal, row.NotificationVersion, []byte(row.ID))
}

// SelectFromNamespace reads one or more rows from namespaces table
func (pdb *db) SelectFromNamespace(
	ctx context.Context,
	filter sqlplugin.NamespaceFilter,
) ([]sqlplugin.NamespaceRow, error) {
	switch {
	case filter.ID != nil || filter.Name != nil:
		if filter.ID != nil && filter.Name != nil {
			return nil, serviceerror.NewInternal("only ID or name filter can be specified for selection")
		}
		return pdb.selectFromNamespace(ctx, filter)
	case filter.PageSize != nil && *filter.PageSize > 0:
		return pdb.selectAllFromNamespace(ctx, filter)
	default:
		return nil, errMissingArgs
	}
}

func (pdb *db) selectFromNamespace(
	ctx context.Context,
	filter sqlplugin.NamespaceFilter,
) ([]sqlplugin.NamespaceRow, error) {
	var err error
	var row sqlplugin.NamespaceRow
	switch {
	case filter.ID != nil:
		err = pdb.conn.GetContext(ctx,
			&row,
			getNamespaceByIDQuery,
			namespaceMetadataSingletonPartitionID,
			[]byte(*filter.ID),
		)
	case filter.Name != nil:
		err = pdb.conn.GetContext(ctx,
			&row,
			getNamespaceByNameQuery,
			namespaceMetadataSingletonPartitionID,
			*filter.Name,
		)
	}
	if err != nil {
		return nil, err
	}
	return []sqlplugin.NamespaceRow{row}, nil
}

func (pdb *db) selectAllFromNamespace(
	ctx context.Context,
	filter sqlplugin.NamespaceFilter,
) ([]sqlplugin.NamespaceRow, error) {
	var err error
	var rows []sqlplugin.NamespaceRow
	switch {
	case filter.GreaterThanID != nil:
		err = pdb.conn.SelectContext(ctx,
			&rows,
			listNamespacesRangeQuery,
			namespaceMetadataSingletonPartitionID,
			[]byte(*filter.GreaterThanID),
			*filter.PageSize,
		)
	default:
		err = pdb.conn.SelectContext(ctx,
			&rows,
			listNamespacesQuery,
			namespaceMetadataSingletonPartitionID,
			filter.PageSize,
		)
	}
	return rows, err
}

// DeleteFromNamespace deletes a single row in namespaces table
func (pdb *db) DeleteFromNamespace(
	ctx context.Context,
	filter sqlplugin.NamespaceFilter,
) (sql.Result, error) {
	var err error
	var result sql.Result
	switch {
	case filter.ID != nil:
		result, err = pdb.conn.ExecContext(ctx,
			deleteNamespaceByIDQuery,
			namespaceMetadataSingletonPartitionID,
			filter.ID,
		)
	default:
		result, err = pdb.conn.ExecContext(ctx,
			deleteNamespaceByNameQuery,
			namespaceMetadataSingletonPartitionID,
			filter.Name,
		)
	}
	return result, err
}

// LockNamespaceMetadata acquires a write lock on a single row in namespace_metadata table
func (pdb *db) LockNamespaceMetadata(
	ctx context.Context,
) (*sqlplugin.NamespaceMetadataRow, error) {
	var row sqlplugin.NamespaceMetadataRow
	err := pdb.conn.GetContext(ctx,
		&row.NotificationVersion,
		lockNamespaceMetadataQuery,
		namespaceMetadataSingletonPartitionID,
	)
	if err != nil {
		return nil, err
	}
	return &row, nil
}

// SelectFromNamespaceMetadata reads a single row in namespace_metadata table
func (pdb *db) SelectFromNamespaceMetadata(
	ctx context.Context,
) (*sqlplugin.NamespaceMetadataRow, error) {
	var row sqlplugin.NamespaceMetadataRow
	err := pdb.conn.GetContext(ctx,
		&row.NotificationVersion,
		getNamespaceMetadataQuery,
		namespaceMetadataSingletonPartitionID,
	)
	return &row, err
}

// UpdateNamespaceMetadata updates a single row in namespace_metadata table
func (pdb *db) UpdateNamespaceMetadata(
	ctx context.Context,
	row *sqlplugin.NamespaceMetadataRow,
) (sql.Result, error) {
	return pdb.conn.ExecContext(ctx,
		updateNamespaceMetadataQuery,
		row.NotificationVersion+1,
		row.NotificationVersion,
		namespaceMetadataSingletonPartitionID,
	)
}
