// The MIT License
//
// Copyright (c) 2021-2022 Martin Lercher.  All rights reserved.
//
// Copyright (c) 2020 Temporal Technologies Inc.  All rights reserved.
//
// Copyright (c) 2020 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package sqlserver_test

import (
	"log"
	"os"
	"path"
	"path/filepath"
	"strings"
	"testing"

	"gitlab.com/lercher/temporal-sqls/sqlserver"

	"github.com/stretchr/testify/suite"
	"go.temporal.io/server/common/config"
	"go.temporal.io/server/common/persistence/persistence-tests"
)

const testTemporalDBName = "temporal1161" // see also cmd\server\config\development.yaml

// getPluginTestClusterOption returns test options
func getPluginTestClusterOption() *persistencetests.TestBaseOptions {
	wd, _ := os.Getwd()
	wd = filepath.ToSlash(wd)
	wd = strings.TrimPrefix(wd, "c:")
	schemadir := path.Clean(path.Join(wd, "../schema"))
	log.Println("SCHEMA:", schemadir)

	_ = os.Setenv("TEMPORAL_ROOT", path.Clean(path.Join(wd, "..")))

	return &persistencetests.TestBaseOptions{
		SQLDBPluginName: sqlserver.PluginName,
		DBUsername:      "",            // use trusted con
		DBPassword:      "logsql=true", // hack: use "logsql=true" to turn sql logging on, everything else turns sql logging off (note: config.SQL has ConnectAttributes for that)
		DBHost:          "localhost",
		DBPort:          1433,               // must not be 0 b/c of "panic: unknown sql store drier: sqlserver" detecting the port
		DBName:          testTemporalDBName, // test suite zeroes this setting delibrately (without patch), the driver panics instead of using "temporal" as the default db name
		SchemaDir:       schemadir,
		StoreType:       config.StoreTypeSQL,
	}
}

// --- adapted from \git\src\github.com\temporalio\temporal\common\persistence\persistence-tests\postgres_test.go ---
// func TestPostgreSQLHistoryV2PersistenceSuite(t *testing.T)
// func TestPostgreSQLMetadataPersistenceSuiteV2(t *testing.T)
// func TestPostgreSQLShardPersistenceSuite(t *testing.T)
// func TestPostgreSQLExecutionManagerSuite(t *testing.T)
// func TestPostgreSQLExecutionManagerWithEventsV2(t *testing.T)
// func TestPostgreSQLClusterMetadataPersistence(t *testing.T)
// func TestPostgreSQLQueuePersistence(t *testing.T) (commented out in source)

// v1.16.1 passes
// v1.7    passes
func TestPluginHistoryV2PersistenceSuite(t *testing.T) {
	s := new(persistencetests.HistoryV2PersistenceSuite)
	s.TestBase = persistencetests.NewTestBaseWithSQL(getPluginTestClusterOption())
	s.TestBase.Setup(nil)
	suite.Run(t, s)
}

// v1.16.1 passes
// v1.7    fails with namespace count 2 instead of 1
// -> that's ok however, b/c we created the default namespace via tctl
func TestPluginMetadataPersistenceSuiteV2(t *testing.T) {
	s := new(persistencetests.MetadataPersistenceSuiteV2)
	s.TestBase = persistencetests.NewTestBaseWithSQL(getPluginTestClusterOption())
	s.TestBase.Setup(nil)
	suite.Run(t, s)
}

// v1.16.1 passes
// v1.7    passes
func TestPluginShardPersistenceSuite(t *testing.T) {
	s := new(persistencetests.ShardPersistenceSuite)
	s.TestBase = persistencetests.NewTestBaseWithSQL(getPluginTestClusterOption())
	s.TestBase.Setup(nil)
	suite.Run(t, s)
}

// v1.16.1 passes
// v1.7    passes
func TestPluginExecutionManagerSuite(t *testing.T) {
	s := new(persistencetests.ExecutionManagerSuite)
	s.TestBase = persistencetests.NewTestBaseWithSQL(getPluginTestClusterOption())
	s.TestBase.Setup(nil)
	suite.Run(t, s)
}

// v1.16.1 passes
// v1.7    passes
func TestPluginExecutionManagerWithEventsV2(t *testing.T) {
	s := new(persistencetests.ExecutionManagerSuiteForEventsV2)
	s.TestBase = persistencetests.NewTestBaseWithSQL(getPluginTestClusterOption())
	s.TestBase.Setup(nil)
	suite.Run(t, s)
}

// v1.16.1 passes
// v1.7    passes
func TestPluginClusterMetadataPersistence(t *testing.T) {
	s := new(persistencetests.ClusterMetadataManagerSuite)
	s.TestBase = persistencetests.NewTestBaseWithSQL(getPluginTestClusterOption())
	s.TestBase.Setup(nil)
	suite.Run(t, s)
}

// v1.16.1 passes as is
// v1.7    passes, if retry policy is increased in \git\src\github.com\temporalio\temporal\common\persistence\persistence-tests\queuePersistenceTest.go
//         func (s *TestBase) Publish(...)
//         and
//         func (s *TestBase) PublishToNamespaceDLQ(...)
func TestPluginQueuePersistence(t *testing.T) {
	s := new(persistencetests.QueuePersistenceSuite)
	s.TestBase = persistencetests.NewTestBaseWithSQL(getPluginTestClusterOption())
	s.TestBase.Setup(nil)
	suite.Run(t, s)
}
