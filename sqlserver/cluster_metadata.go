// The MIT License
//
// Copyright (c) 2021-2022 Martin Lercher.  All rights reserved.
//
// Copyright (c) 2020 Temporal Technologies Inc.  All rights reserved.
//
// Copyright (c) 2020 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package sqlserver

import (
	"context"
	"database/sql"
	"fmt"
	"strconv"
	"strings"

	p "go.temporal.io/server/common/persistence"

	"go.temporal.io/server/common/persistence/sql/sqlplugin"
)

// go-mssqldb doesn't like untyped int as parameters, so we give them the known type int64 ->
// "Unable to start server: unable to initialize cluster metadata: error while fetching cluster metadata: GetClusterMetadata operation failed. Error: mssql: unknown type for int"
const constMetadataPartition int64 = 0
const constMembershipPartition int64 = 0

const (
	// ****** CLUSTER_METADATA TABLE ******
	insertClusterMetadataQry = `INSERT INTO temporal.cluster_metadata_info (metadata_partition, cluster_name, data, data_encoding, version) VALUES (@p1,@p2,@p3,@p4,@p5)`

	updateClusterMetadataQry = `UPDATE temporal.cluster_metadata_info SET data=@p1,data_encoding=@p2,version=@p3 WHERE metadata_partition=@p4 and cluster_name=@p5`

	getClusterMetadataBase              = `SELECT data, data_encoding, version FROM temporal.cluster_metadata_info `
	getClusterMetadataBaseLimitTemplate = `SELECT TOP %d data, data_encoding, version FROM temporal.cluster_metadata_info `
	getClusterMetadataQry               = getClusterMetadataBase + `WHERE metadata_partition=@p1 AND cluster_name=@p2`
	listClusterMetadataQryTemplate      = getClusterMetadataBaseLimitTemplate + `WHERE metadata_partition=@p1 ORDER BY cluster_name`
	listClusterMetadataRangeQryTemplate = getClusterMetadataBaseLimitTemplate + `WHERE metadata_partition=@p1 AND cluster_name>@p2 ORDER BY cluster_name`

	writeLockGetClusterMetadataQry = `SELECT data, data_encoding, version FROM temporal.cluster_metadata_info WITH (UPDLOCK) WHERE metadata_partition=@p1 and cluster_name=@p2`

	deleteClusterMetadataQry = `DELETE FROM temporal.cluster_metadata_info WHERE metadata_partition = @p1 AND cluster_name = @p2`

	// ****** CLUSTER_MEMBERSHIP TABLE ******
	templateUpsertActiveClusterMembership = `BEGIN TRAN
UPDATE temporal.cluster_membership SET 
	rpc_address=@p3, 
	rpc_port=@p4, 
	role=@p5, 
	session_start=@p6, 
	last_heartbeat=@p7, 
	record_expiry=@p8
	WHERE membership_partition=@p1 and host_id=@p2
IF @@ROWCOUNT = 0 INSERT INTO temporal.cluster_membership (membership_partition, host_id, rpc_address, rpc_port, role, session_start, last_heartbeat, record_expiry)
VALUES (@p1,@p2,@p3,@p4,@p5,@p6,@p7,@p8)
COMMIT`

	templatePruneStaleClusterMembership = `DELETE FROM temporal.cluster_membership 
WHERE host_id IN (SELECT host_id FROM temporal.cluster_membership WHERE membership_partition=@p1 AND record_expiry<@p2)`

	// pg's and mysql's ... LIMIT $n suffix needs to be replaced either by a "SELECT TOP (@pn) ... FROM ..." or more directly with a "SELECT TOP %d ... FROM ..." statement, where %d is replaced in the query string
	templateGetClusterMembershipWithoutLimit = `SELECT host_id, rpc_address, rpc_port, role, session_start, last_heartbeat, record_expiry FROM temporal.cluster_membership WHERE membership_partition = @p`        // param number is appended
	templateGetClusterMembershipWithLimit    = `SELECT TOP %d host_id, rpc_address, rpc_port, role, session_start, last_heartbeat, record_expiry FROM temporal.cluster_membership WHERE membership_partition = @p` // param number is appended

	// ClusterMembership WHERE Suffixes, must trail with @p, b/c an int representing the parameter number is appended
	templateWithRoleSuffix           = ` AND role=@p`
	templateWithHeartbeatSinceSuffix = ` AND last_heartbeat>@p`
	templateWithRecordExpirySuffix   = ` AND record_expiry>@p`
	templateWithRPCAddressSuffix     = ` AND rpc_address=@p`
	templateWithHostIDSuffix         = ` AND host_id=@p`
	templateWithHostIDGreaterSuffix  = ` AND host_id>@p`
	templateWithSessionStartSuffix   = ` AND session_start>=@p`

	// Generic SELECT Suffixes
	templateWithOrderBySessionStartSuffix = ` ORDER BY membership_partition ASC, host_id ASC`
)

func (pdb *db) SaveClusterMetadata(
	ctx context.Context,
	row *sqlplugin.ClusterMetadataRow,
) (sql.Result, error) {
	if row.Version == 0 {
		result, err := pdb.conn.ExecContext(ctx,
			insertClusterMetadataQry,
			constMetadataPartition,
			row.ClusterName,
			row.Data,
			row.DataEncoding,
			1,
		)
		return result, err
	}
	return pdb.conn.ExecContext(ctx,
		updateClusterMetadataQry,
		row.Data,
		row.DataEncoding,
		row.Version+1,
		constMetadataPartition,
		row.ClusterName,
	)
}

func (pdb *db) ListClusterMetadata(
	ctx context.Context,
	filter *sqlplugin.ClusterMetadataFilter,
) ([]sqlplugin.ClusterMetadataRow, error) {
	var err error
	var rows []sqlplugin.ClusterMetadataRow
	switch {
	case len(filter.ClusterName) != 0:
		listClusterMetadataRangeQry := fmt.Sprintf(listClusterMetadataRangeQryTemplate, *filter.PageSize)
		err = pdb.conn.SelectContext(ctx,
			&rows,
			listClusterMetadataRangeQry,
			constMetadataPartition,
			filter.ClusterName,
		)
	default:
		listClusterMetadataQry := fmt.Sprintf(listClusterMetadataQryTemplate, *filter.PageSize)
		err = pdb.conn.SelectContext(ctx,
			&rows,
			listClusterMetadataQry,
			constMetadataPartition,
		)
	}
	return rows, err
}

func (pdb *db) GetClusterMetadata(
	ctx context.Context,
	filter *sqlplugin.ClusterMetadataFilter,
) (*sqlplugin.ClusterMetadataRow, error) {
	var row sqlplugin.ClusterMetadataRow
	err := pdb.conn.GetContext(ctx,
		&row,
		getClusterMetadataQry,
		constMetadataPartition,
		filter.ClusterName,
	)
	if err != nil {
		return nil, err
	}
	return &row, err
}

func (pdb *db) WriteLockGetClusterMetadata(
	ctx context.Context,
	filter *sqlplugin.ClusterMetadataFilter,
) (*sqlplugin.ClusterMetadataRow, error) {
	var row sqlplugin.ClusterMetadataRow
	err := pdb.conn.GetContext(ctx,
		&row,
		writeLockGetClusterMetadataQry,
		constMetadataPartition,
		filter.ClusterName,
	)
	if err != nil {
		return nil, err
	}
	return &row, nil
}

func (pdb *db) DeleteClusterMetadata(
	ctx context.Context,
	filter *sqlplugin.ClusterMetadataFilter,
) (sql.Result, error) {

	return pdb.conn.ExecContext(ctx,
		deleteClusterMetadataQry,
		constMetadataPartition,
		filter.ClusterName,
	)
}

func (pdb *db) UpsertClusterMembership(
	ctx context.Context,
	row *sqlplugin.ClusterMembershipRow,
) (sql.Result, error) {
	return pdb.conn.ExecContext(ctx,
		templateUpsertActiveClusterMembership,
		constMembershipPartition,
		row.HostID,
		row.RPCAddress,
		row.RPCPort,
		row.Role,
		row.SessionStart,
		row.LastHeartbeat,
		row.RecordExpiry)
}

func (pdb *db) GetClusterMembers(
	ctx context.Context,
	filter *sqlplugin.ClusterMembershipFilter,
) ([]sqlplugin.ClusterMembershipRow, error) {
	var queryString strings.Builder
	var operands []interface{}

	if filter.MaxRecordCount > 0 {
		templateGetClusterMembership := fmt.Sprintf(templateGetClusterMembershipWithLimit, filter.MaxRecordCount)
		queryString.WriteString(templateGetClusterMembership)
	} else {
		queryString.WriteString(templateGetClusterMembershipWithoutLimit)
	}
	operands = append(operands, constMembershipPartition)
	queryString.WriteString(strconv.Itoa(len(operands)))

	if filter.HostIDEquals != nil {
		queryString.WriteString(templateWithHostIDSuffix)
		operands = append(operands, filter.HostIDEquals)
		queryString.WriteString(strconv.Itoa(len(operands)))
	}

	if filter.RPCAddressEquals != "" {
		queryString.WriteString(templateWithRPCAddressSuffix)
		operands = append(operands, filter.RPCAddressEquals)
		queryString.WriteString(strconv.Itoa(len(operands)))
	}

	if filter.RoleEquals != p.All {
		queryString.WriteString(templateWithRoleSuffix)
		operands = append(operands, filter.RoleEquals)
		queryString.WriteString(strconv.Itoa(len(operands)))
	}

	if !filter.LastHeartbeatAfter.IsZero() {
		queryString.WriteString(templateWithHeartbeatSinceSuffix)
		operands = append(operands, filter.LastHeartbeatAfter)
		queryString.WriteString(strconv.Itoa(len(operands)))
	}

	if !filter.RecordExpiryAfter.IsZero() {
		queryString.WriteString(templateWithRecordExpirySuffix)
		operands = append(operands, filter.RecordExpiryAfter)
		queryString.WriteString(strconv.Itoa(len(operands)))
	}

	if !filter.SessionStartedAfter.IsZero() {
		queryString.WriteString(templateWithSessionStartSuffix)
		operands = append(operands, filter.SessionStartedAfter)
		queryString.WriteString(strconv.Itoa(len(operands)))
	}

	if filter.HostIDGreaterThan != nil {
		queryString.WriteString(templateWithHostIDGreaterSuffix)
		operands = append(operands, filter.HostIDGreaterThan)
		queryString.WriteString(strconv.Itoa(len(operands)))
	}

	queryString.WriteString(templateWithOrderBySessionStartSuffix)

	compiledQryString := queryString.String()

	var rows []sqlplugin.ClusterMembershipRow
	err := pdb.conn.SelectContext(ctx, &rows,
		compiledQryString,
		operands...)

	if err != nil {
		return nil, err
	}

	convertedRows := make([]sqlplugin.ClusterMembershipRow, 0, len(rows))
	for _, r := range rows {
		r.SessionStart = r.SessionStart.UTC()
		convertedRows = append(convertedRows, r)
	}
	return convertedRows, err
}

func (pdb *db) PruneClusterMembership(
	ctx context.Context,
	filter *sqlplugin.PruneClusterMembershipFilter,
) (sql.Result, error) {
	return pdb.conn.ExecContext(ctx,
		templatePruneStaleClusterMembership,
		constMembershipPartition,
		filter.PruneRecordsBefore,
	)
}
