// The MIT License
//
// Copyright (c) 2021-2022 Martin Lercher.  All rights reserved.
//
// Copyright (c) 2020 Temporal Technologies Inc.  All rights reserved.
//
// Copyright (c) 2020 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package sqlserver

import (
	"context"

	mssql "github.com/denisenkom/go-mssqldb"
	"github.com/jmoiron/sqlx"

	"go.temporal.io/server/common/persistence/schema"
	"go.temporal.io/server/common/persistence/sql/sqlplugin"
)

// db represents a logical connection to an mssql database (receiver name "pdb" = "Plugin-Data-Base")
type db struct {
	dbKind    sqlplugin.DbKind
	dbName    string
	db        *sqlx.DB
	tx        *sqlx.Tx
	conn      sqlplugin.Conn
	converter DataConverter
}

var _ sqlplugin.DB = (*db)(nil)
var _ sqlplugin.Tx = (*db)(nil)

// IsDupEntryError indicates a duplicate primary key i.e. the row already exists,
func (pdb *db) IsDupEntryError(err error) bool {
	return isDupEntryError(err)
}

func isDupEntryError(err error) bool {
	const (
		// https://stackoverflow.com/questions/12581230/which-error-code-is-return-in-sql-server-when-inserting-duplicate-value-in-prima
		// For anyone looking over this in the future. a 2627 is VERY different from a 2601. A 2601 is a unique key violation A 2627 is a primary key violation.
		sqlServerUniqueKeyViolation  = 2601
		sqlServerPrimaryKeyViolation = 2627
	)
	if sqlErr, ok := err.(mssql.Error); ok {
		return sqlErr.Number == sqlServerUniqueKeyViolation || sqlErr.Number == sqlServerPrimaryKeyViolation
	}
	return false
}

// newDB returns an instance of DB, which is a logical
// connection to the underlying mssql database
func newDB(
	dbKind sqlplugin.DbKind,
	dbName string,
	xdb *sqlx.DB,
	tx *sqlx.Tx,
) *db {
	mdb := &db{
		dbKind: dbKind,
		dbName: dbName,
		db:     xdb,
		tx:     tx,
	}
	mdb.conn = xdb
	if tx != nil {
		mdb.conn = tx
	}
	mdb.converter = &converter{}
	return mdb
}

// BeginTx starts a new transaction and returns a reference to the Tx object
func (pdb *db) BeginTx(ctx context.Context) (sqlplugin.Tx, error) {
	xtx, err := pdb.db.BeginTxx(ctx, nil)
	if err != nil {
		return nil, err
	}
	return newDB(pdb.dbKind, pdb.dbName, pdb.db, xtx), nil
}

// Commit commits a previously started transaction
func (pdb *db) Commit() error {
	return pdb.tx.Commit()
}

// Rollback triggers rollback of a previously started transaction
func (pdb *db) Rollback() error {
	return pdb.tx.Rollback()
}

// Close closes the connection to the mysql db
func (pdb *db) Close() error {
	return pdb.db.Close()
}

// PluginName returns the name of the mysql plugin
func (pdb *db) PluginName() string {
	return PluginName
}

// ExpectedVersion returns expected version of the dbKind of pdb.
func (pdb *db) ExpectedVersion() string {
	switch pdb.dbKind {
	case sqlplugin.DbKindMain:
		return "1.8"
	case sqlplugin.DbKindVisibility:
		return "1.8" // temporal is on 1.1 here, but we have visibility and main in the same DB and hence the same version
	default:
		return ""
	}
}

// VerifyVersion verify schema version is up to date
func (pdb *db) VerifyVersion() error {
	expectedVersion := pdb.ExpectedVersion()
	return schema.VerifyCompatibleVersion(pdb, pdb.dbName, expectedVersion)
}
