// The MIT License
//
// Copyright (c) 2021-2022 Martin Lercher.  All rights reserved.
//
// Copyright (c) 2020 Temporal Technologies Inc.  All rights reserved.
//
// Copyright (c) 2020 Uber Technologies, Inc.
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

package sqlserver

import (
	"time"
)

var (
	minSQLServerDateTime = getMinSQLServerDateTime()
)

type (
	// DataConverter defines the API for conversions to/from
	// go types and this driver's datatypes. Time is in UTC.
	DataConverter interface {
		ToServerDateTime(t time.Time) time.Time
		FromServerDateTime(t time.Time) time.Time
	}
	converter struct{}
)

// ToServerDateTime converts time to SQL Server datetime
func (c *converter) ToServerDateTime(t time.Time) time.Time {
	if t.IsZero() {
		return minSQLServerDateTime
	}
	return t.UTC()
}

// FromServerDateTime converts SQL Server datetime and returns go time
func (c *converter) FromServerDateTime(t time.Time) time.Time {
	// NOTE: PostgreSQL will preserve the location of time in a
	//  weird way, here need to call UTC to remove the time location
	if t.Equal(minSQLServerDateTime) {
		return time.Time{}.UTC()
	}
	return t.UTC()
}

func getMinSQLServerDateTime() time.Time {
	// The minimum valid date for a SqlDateTime structure is January 1, 1753
	// To avoid TZ and summer time problems we use a year later
	t, err := time.Parse(time.RFC3339, "1754-01-01T00:00:00Z")
	if err != nil {
		return time.Unix(0, 0).UTC()
	}
	return t.UTC()
}
